export class MetaModel {
    attributes: Array<{
        type: string,
        width: number,
        label: string,
        options: Array<string>
    }>;
    pagination: {
        pageCount: number,
        perPage: number,
        totalCount: number
    };
    sort: Array<string>;
    filter: Array<string>;
    title: string;
}
