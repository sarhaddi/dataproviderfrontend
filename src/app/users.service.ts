import {HttpClient, HttpParams, HttpResponse} from '@angular/common/http';
import {Injectable} from '@angular/core';
import {MatTableDataSource} from '@angular/material/table';
import {MetaModel} from './meta.model';
import {Sort} from '@angular/material/sort';

@Injectable({
    providedIn: 'root'
})
export class UsersService {
    data = [];
    meta: MetaModel;
    page = 1;
    perPage = 0;
    attributes: Array<string> = [];
    sort: Sort;
    dataSource = new MatTableDataSource(this.data);
    filter = {};

    constructor(
        private http: HttpClient
    ) {
    }

    loadMeta(): void {
        this.http.get('http://localhost:8080/site/meta').subscribe((response: MetaModel) => {
            this.meta = response;
            this.attributes = this.getColumns();
            this.load();
        });
    }

    load(): void {
        let params = new HttpParams();
        params = params.append('page', this.page.toString());
        if (this.perPage) {
            params = params.append('per-page', this.perPage.toString());
        }
        for (const key of Object.keys(this.filter)) {
            if (this.filter[key]) {
                params = params.append(key, this.filter[key]);
            }
        }
        if (this.sort && this.sort.active !== undefined && this.sort.direction !== '') {
            params = params.append('sort', (this.sort.direction === 'desc' ? '-' : '') + this.sort.active);
        }
        if (this.attributes.length) {
            params = params.append('attributes', this.attributes.join(','));
        }
        this.http.get('http://localhost:8080/site/data', {
            params,
            observe: 'response'
        }).subscribe((response: HttpResponse<any>) => {
            this.data = response.body;
            this.page = +response.headers.get('X-Pagination-Current-Page');
            this.meta.pagination.perPage = +response.headers.get('X-Pagination-Per-Page');
            this.meta.pagination.totalCount = +response.headers.get('X-Pagination-Total-Count');
            this.meta.pagination.pageCount = +response.headers.get('X-Pagination-Page-Count');
            this.dataSource = new MatTableDataSource(this.data);
        });
    }

    changePage(page): void {
        this.page = page.pageIndex + 1;
        this.perPage = page.pageSize;
        this.load();
    }

    changeSort(sort): void {
        this.sort = sort;
        this.load();
    }

    getColumns(): Array<string> {
        if (this.meta.attributes === undefined) {
            return [];
        }
        return Object.keys(this.meta.attributes);
    }

    getType(attr): string {
        switch (this.meta.attributes[attr].type) {
            default:
            case 'string':
                return 'text';
            case 'integer':
            case 'float':
                return 'number';
            case 'date':
                return 'date';
            case 'enum':
                return 'select';
        }
    }

    toggleAttribute(attr): void {
        const index = this.attributes.indexOf(attr);
        if (index === -1) {
            this.attributes.push(attr);
        } else {
            this.attributes.splice(index, 1);
        }
        this.load();
    }
}
